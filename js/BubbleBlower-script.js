"use strict";
document.addEventListener("DOMContentLoaded",function(){
	hw_game_BubbleBlower();
});


function hw_game_BubbleBlower(){
	var hwg = {};// глобальный объект для работы
	hwg.win = {}; // тут все кнопки и т.д буду хранится
	hwg.fps = 60; // отвечает за ФПС игры ( 60 = 60 кадров в секунду)
	hwg.stat = 'pause'; // текущее состояние игры
	hwg.lang = localStorage.getItem('lang') || 'en'; // Текущий язык игры
	hwg.ajaxUrl = { // Путь для связи с аяксовыми запросами
		server:'http://80.243.140.158:88/bubblePop', // Путь до самого сервера
		ratingList:'/list', // Запрос на рейтинг
		ratingAdd:'/addUser', // Добавит ползователя в рейтинк
	};
		
	hwg.idTick = 0; // ИД текущего тика игры
	hwg.timer = {}; // ТАЙМЕР игрового процесса и все его характеристики
		hwg.timer.timeMax = 30; // максимальное время игры в Секундах
		hwg.timer.time = 0; // текущее время в МилиСекундах
		
	hwg.bubbles = {}; // тут все пузырьки хранятся
	hwg.idb = 0; // итератор для ЙД пузырьков
	hwg.bubblesSize = {min:8,max:18}; // размеры пузырька в %
	hwg.bubblesKfCreate = 12; // Коофицент появления пузырьков
	hwg.bubblesKofSpeed = 0.4; // Коофицент Скорости 
	hwg.bubblesMaxInterval = 100; // Максимальный интервал между созданиями пузырьков
	hwg.score = 0; // текущий счет 
	hwg.scoreAjax = 0; // текущий счет 
	hwg.scils = {} // переменная для работы с активными скилами
	hwg.audio = {} // различные аудио эфекты для игры
	hwg.namePlayer = localStorage.getItem('name_player') || ''; // Имя игрока

	hwg.win.mainG = document.querySelector('#hw-game-BubbleBlower');
	// Кнопки
	hwg.win.play = document.querySelector('#hw-btn-play'); // кнопка запуска игры
	hwg.win.lightning = document.querySelector('#hw-btn-lightning'); // кнопка Молнии
	hwg.win.incision  = document.querySelector('#hw-btn-incision'); // кнопка Линии
	hwg.win.explosion = document.querySelector('#hw-btn-explosion'); // кнопка Взрыва
	hwg.win.commercial = document.querySelector('#hw-btn-commercial-play'); // кнопка Вызова рекламы
	hwg.win.menu = document.querySelectorAll('.hw-but-menu'); //Кнопочки меню
	hwg.win.ratingClouse = document.querySelector('.hw-rating-clouse'); //Кнопка закрытия Ретинга
	hwg.win.ratingClouseBack = document.querySelectorAll('.hw-menu-clouse-back'); //Кнопка закрытия фоновое окно
	hwg.win.menuBack = document.querySelector('.hw-menu .hw-menu-but-back'); //кнопка назад
	hwg.win.menuRating = document.querySelector('.hw-menu .hw-menu-but-rating'); //кнопка рейтинга
	hwg.win.menuSound = document.querySelector('.hw-menu .hw-menu-but-sound'); //кнопка звука
	hwg.win.menuLang = document.querySelector('.hw-menu .hw-menu-but-lang'); //кнопка языка
	hwg.win.menuMusick = document.querySelector('.hw-menu .hw-menu-but-musick'); //кнопка музыки
	hwg.win.finisBack = document.querySelector('.hw-finish .hw-finish-but-back'); //кнопка назад
	hwg.win.finisRating = document.querySelector('.hw-finish .hw-finish-but-rating'); //отправить в рейтинг
	hwg.win.modalClouse = document.querySelectorAll('.hw_modal_clouse'); //Кнопка закрытия модальных окон
	hwg.win.clouseСommercial = document.querySelector('.hw_clouse_commercial'); //Кнопка закрытия Окна рекламы
	hwg.win.info = document.querySelector('#hw-btn-info'); // Открывает информацию о том как играть ( хелпер )
	// hwg.win.menuExit = document.querySelector('.hw-menu .hw-menu-but-exit'); //кнопка выхода 

	hwg.win.gameField = document.querySelector('.hw-game-field'); // игровое поле
	hwg.win.gameTImer = document.querySelector('.hw-but-timer'); // поля для обратного отсчета
	hwg.win.gameScore = document.querySelector('.hw-but-score'); // поля для Текущих набитых очков
	hwg.win.fullscrin = document.querySelectorAll('.hw-but-fullscrin'); // Раскрывает на весь экран	
	hwg.win.rating = document.querySelector('.hw-rating'); //Раздел Ретинга
	hwg.win.menuWin = document.querySelector('.hw-menu'); //Раздел меню
	hwg.win.finishWin = document.querySelector('.hw-finish'); //Раздел Финишного окна
	hwg.win.finisScore = document.querySelector('.hw-finish .hw-finish-but-score'); //текущий результат
	hwg.win.finisError = document.querySelector('.hw-finish .hw-finish-but-error'); //Сообщение об ошибки
	hwg.win.finisName = document.querySelector('.hw-finish .hw-finish-but-name'); //окно для воода имени
	hwg.win.commercialBlock = document.querySelector('#commercial-modal-block'); //Окно комерции
	hwg.win.addSkillBlock = document.querySelector('#add-random-skill'); // Окно добавления нового скила
	hwg.win.blockedDisplay = document.querySelector('#blocked-display'); // Окно Для блокирования экрана
	hwg.win.loaderDisplay = document.querySelector('#loader-display'); // Окно для загрузчика
	hwg.win.modalInfo = document.querySelector('#hw-modal-info'); // Окно Для блокирования экрана
	
	/* Animation */
	hwg.win.animEndLine = document.querySelectorAll('.anim-end-line'); // Линии для анимации окончания игры
	hwg.win.animlightningBlock = document.querySelector('#lightning-block'); // Блок Молнии
	hwg.win.animIncisionBlock = document.querySelector('#incision-block'); // Блок Линии разреза
	hwg.win.animExplosionBlock = document.querySelector('#explosion-block'); // Блок Взрыва
	
	// объект спрайтов, и переменных для них
	hwg.sprites = {}
	hwg.sprites.play = 'play';
	hwg.sprites.pause = 'pause';
	hwg.sprites.soundControl = 'on';
	hwg.sprites.arr = [//  перечень картинок что летят в верх
		'bubbles/babble_1.png','bubbles/babble_1.png','bubbles/babble_2.png','bubbles/babble_3.png',
		'bubbles/babble_4.png','bubbles/babble_5.png','bubbles/babble_6.png','bubbles/babble_7.png',
		'bubbles/babble_8.png','bubbles/babble_9.png','bubbles/babble_10.png','bubbles/babble_11.png'
	]; 
	hwg.sprites.boom = [ // эфекты уничтожения
		{type:'boom',img:'boom/boom.png',sound:'audio/boom.mp3'},
		{type:'boom',img:'boom/boom.png',sound:'audio/boom.mp3'},
		{type:'line',img:'boom/line.png',sound:'audio/line.mp3'},
		{type:'flash',img:'boom/flash.png',sound:'audio/flash.mp3'},
	]; 
	hwg.sprites.dir = 'img/'; //  папка где находятся картинки
	
	// Контроллер скилов
	hwg.scils = {
		lightning: {
			count: (parseInt(localStorage.getItem('lightning_count')) || 8),
			timid: 0,
			audio: new Audio('audio/lightning.mp3')
		},
		incision: {
			count:(parseInt(localStorage.getItem('incision_count')) || 8),
			timid:0,
			audio: new Audio('audio/incision.mp3')
		},
		explosion: {
			count:(parseInt(localStorage.getItem('explosion_count')) || 8),
			timid:0,
			audio: new Audio('audio/explosion.mp3')
		},
		timerStop: 400, // время на анимацию, и паузу игры 
	}
	
	// Аудио эфекты
	hwg.musickControl = localStorage.getItem('musick_сontrol') || 'on';
	hwg.audio.fone =  new Audio('audio/fon_1.mp3'); // Фоновая мелодия
	hwg.audio.endGame =  new Audio('audio/end_game.mp3'); // звук конца игры
	

	// нажатие на кнопку играть
	hwg.win.play.addEventListener('pointerup',click_but_start);
	//нажатие на кнопку НАВЕСЬ экран  fullscrin
	hwg.win.fullscrin.forEach(function(elem){
		elem.addEventListener('pointerup',click_but_fullscrin);
	});
	//нажатие на кнопку меню menu
	hwg.win.menu.forEach(function(elem){
		elem.addEventListener('pointerdown',click_but_menu);
	});
	//нажатие на кнопку фонового экрана за меню 
	hwg.win.ratingClouseBack.forEach(function(elem){
		elem.addEventListener('pointerdown',click_but_menu);
	});	
	//нажатие на кнопку закрытия модального окна
	hwg.win.modalClouse.forEach(function(elem){
		elem.addEventListener('click',click_but_clouse_modal);
	});
	// нажатие на кнопки меню
	hwg.win.menuBack.addEventListener('pointerup',click_but_menuBack);
	hwg.win.menuRating.addEventListener('pointerup',click_but_menuRating);
	hwg.win.ratingClouse.addEventListener('pointerup',click_but_clouseRating);
	hwg.win.menuSound.addEventListener('pointerup',click_but_menuSound);
	hwg.win.menuMusick.addEventListener('pointerup',click_but_menuMusick);
	hwg.win.menuLang.addEventListener('pointerup',click_but_lang);
	hwg.win.info.addEventListener('click',click_btn_info);
	// hwg.win.menuExit.addEventListener('pointerup',click_but_menuExit);
	// нажатие на кнопки финишного окна
	hwg.win.finisScore.addEventListener('pointerup',click_but_finisScore);
	hwg.win.finisName.addEventListener('pointerup',click_but_finisName);
	hwg.win.finisRating.addEventListener('pointerup',click_but_finisRating);
	hwg.win.finisBack.addEventListener('pointerup',click_but_finisBack);
	// Нажатие на активные кнопки атаки
	hwg.win.lightning.addEventListener('click',click_but_lightning);
	hwg.win.incision.addEventListener ('click',click_but_incision);
	hwg.win.explosion.addEventListener('click',click_but_explosion);
	// Нажатие на кнопки комерции ( рекламы )
	hwg.win.commercial.addEventListener('click',click_but_commercial);
	hwg.win.clouseСommercial.addEventListener('click',click_btn_clouse_commercial);
	document.body.addEventListener('click',()=>{
		if (hwg.musickControl == 'on') {hwg.audio.fone.play()}
	});

	// Функция инициализации всего игрового поля
	constructor();
	
	// создать пузырёк 
	function constructor(){
		create_bubble(35,40); // создать пузырёк
		hwg.score = 0;
		set_count_item_skills()
		hwg.audio.fone.autoplay = true;
		hwg.audio.fone.loop = true
		hwg.audio.fone.volume = 0.4;

		// Если аудио возможно запустить запустим
		if (hwg.musickControl == 'on') {
			let promise = hwg.audio.fone.play();
			if (promise !== undefined) {
			promise.then(_ => {
				hwg.audio.fone.play()
			}).catch(error => {
				// Autoplay was prevented.
				// Show a "Play" button so that user can start playback.
			});
			}
		}
		
		// если окно неактивно
		document.addEventListener("visibilitychange", () => {
		  if (document.visibilityState === 'visible') {
			if (hwg.musickControl == 'on') {hwg.audio.fone.play();}
		  } else {
			hwg.audio.fone.pause();
			pause_game();
		  }
		});
		// Проерим включен ли звук и перерисуем кнопочку
		if (hwg.musickControl == 'on'){ // если класс есть
			hwg.win.menuMusick.querySelector('span').innerHTML = "ON";
			hwg.win.menuMusick.classList.remove('active');
		}else {// если класса нету
			hwg.win.menuMusick.querySelector('span').innerHTML = "OFF";
			hwg.win.menuMusick.classList.add('active');
		}
		
		// Языковой момент
		correct_lang();
	}
	
	// Установит нужный язык
	function correct_lang(){
		hwg.win.menuBack.querySelector('lang').innerHTML = LANG.btn_back[hwg.lang]
		hwg.win.finisBack.querySelector('lang').innerHTML = LANG.btn_back[hwg.lang]
		hwg.win.menuRating.querySelector('lang').innerHTML = LANG.btn_rating[hwg.lang]
		hwg.win.menuSound.querySelector('lang').innerHTML = LANG.btn_sound[hwg.lang]
		hwg.win.menuMusick.querySelector('lang').innerHTML = LANG.btn_musick[hwg.lang]
		hwg.win.menuLang.querySelector('lang').innerHTML = LANG.btn_lang[hwg.lang]
		hwg.win.menuLang.dataset.lang = hwg.lang
		document.body.querySelector('#hw-lang-pref').innerHTML = hwg.lang
		hwg.win.finisRating.querySelector('lang').innerHTML = LANG.btn_go_rating[hwg.lang]
		hwg.win.addSkillBlock.querySelector('lang').innerHTML = LANG.btn_get_skill[hwg.lang]
		hwg.win.modalInfo.querySelector('lang').innerHTML = LANG.modal_info[hwg.lang]
	}
	
	// установит в скилы верное количество итемов
	function set_count_item_skills(){
		hwg.scils.lightning.count = (parseInt(localStorage.getItem('lightning_count')) || 8);
		hwg.scils.incision.count = (parseInt(localStorage.getItem('incision_count')) || 8);
		hwg.scils.explosion.count = (parseInt(localStorage.getItem('explosion_count')) || 8);
		
		hwg.win.lightning.querySelector('.count').innerHTML = hwg.scils.lightning.count
		hwg.win.incision.querySelector('.count').innerHTML = hwg.scils.incision.count
		hwg.win.explosion.querySelector('.count').innerHTML = hwg.scils.explosion.count
	}

	// нажатие на кнопку старта
	function click_but_start(){
		if (hwg.stat == 'pause'){// СТАРТ игры
			star_game(); // запуск игры
		}else if (hwg.stat == 'play'){ //ПАУЗА игры
			pause_game(); // остановка игры
		}else if (hwg.stat == 'stop'){
			star_game();
		} 
	}
	// нажатие на кнопку НА весь экран 
	function click_but_fullscrin(){
		var active = this.classList.contains('active')
		if (active){ // если класс есть
			hwg.win.fullscrin.forEach(function(elem){
				elem.classList.remove('active');
			});
			fullScreen('clouse');
		}else {// если класса нету
			hwg.win.fullscrin.forEach(function(elem){
				elem.classList.add('active');
			});
			fullScreen('open');
		}
	}
	// нажатие на кнопку меню
	function click_but_menu(){
		var active = hwg.win.menuWin.classList.contains('active')
		if (active){ // если класс есть
			hwg.win.menuWin.classList.remove('active');
			hwg.win.menu.forEach((elem) => { elem.classList.remove('active');});
		}else {// если класса нету
			hwg.win.menuWin.classList.add('active');
			hwg.win.menu.forEach((elem) => {elem.classList.add('active');});
		}
	}
	// запускает игру
	hwg.timer.bubblesStepSec = 0;
	function star_game(){
		if(hwg.stat != 'play'){
			if(!hwg.score){hwg.score = 0;}
			if(hwg.score === NaN){hwg.score = 0;}
			console.log('Game Start');
			hwg.win.play.classList.add('active');
			hwg.win.mainG.classList.add('play');
			hwg.win.gameScore.innerHTML = hwg.score || 0;
			hwg.stat = 'play';

			const bufftimeMax = hwg.timer.timeMax*1000;
			const stepKof = hwg.bubblesKfCreate / hwg.timer.timeMax;
			let stepSec = 0;
			tick(function(aks){
				if(aks >= bufftimeMax) {
					stop_game()
				} else {
					hwg.win.gameTImer.innerHTML = hwg.timer.timeMax-parseInt(aks/1000);

					// Создание нового пузырька
					if(aks >= hwg.timer.bubblesStepSec ) {
						create_bubble();
						stepSec = 1000-((parseInt(aks) * stepKof) / 10);
						hwg.timer.bubblesStepSec+= (stepSec > hwg.bubblesMaxInterval)? stepSec : hwg.bubblesMaxInterval ;
					}

					// Поднятие пузырька вверх, и удаление если достиг предела
					for(var key in hwg.bubbles){
						const item = hwg.bubbles[key];
						if (item.top <= -10){
							hwg.win.gameField.removeChild(item.elem);
							delete hwg.bubbles[key];
						}else {
							item.top-= (item.kof*0.1);
							item.elem.style.top = item.top+'%';
						}
					};
				}
			});
		}
	}

	// приостанавливает игру
	function pause_game(playControl = true){
		console.log('Game Pause');
		hwg.stat = 'pause';
		playControl && hwg.win.mainG.classList.remove('play');
		hwg.win.play.classList.remove('active');
		cancelAnimationFrame(hwg.idTick);
	}
	// останавливает игру
	function stop_game(){
		console.log('Game Stop');
		hwg.win.blockedDisplay.classList.add('active');
		hwg.stat = 'stop';
		hwg.win.mainG.classList.remove('play');
		hwg.scoreAjax = hwg.score;
		hwg.win.finisScore.innerHTML = hwg.score;
		hwg.win.gameTImer.innerHTML = 0 ;
		cancelAnimationFrame(hwg.idTick);
		hwg.timer.time = 0;
		hwg.score = 0;
		hwg.timer.bubblesStepSec = 0;
		clear_pole();//функция очищает игровое поле
		hwg.audio.endGame.play();
		
		hwg.win.animEndLine.forEach(elem=>{elem.classList.add('active')});
		setTimeout(()=>{
			hwg.win.animEndLine.forEach(elem=>{elem.classList.remove('active')});
			hwg.win.blockedDisplay.classList.remove('active');
			
			hwg.win.play.classList.remove('active');
			hwg.win.lightning.classList.remove('disable');
			hwg.win.incision.classList.remove('disable');
			hwg.win.explosion.classList.remove('disable');
			
			create_bubble(35,40);
			hwg.win.finisError.style.opacity = '0';
			hwg.win.finisName.style.display = 'list-item';
			hwg.win.finisRating.style.display = 'list-item';
			hwg.win.finishWin.classList.add("active");
			

			if(hwg.namePlayer){
				hwg.win.finisName.innerHTML = hwg.namePlayer
			} else {
				hwg.win.finisName.querySelector('input').value = '';
			}
		},800);
	}
	
	// создаст пузырик
	function create_bubble(posX=false,posY=false){
		var cssBub = '';
		var kofSpeed = get_random_int(1,((hwg.timer.time/1000)+1)) * hwg.bubblesKofSpeed;
		var ide = hwg.idb++;
		
		var mainSize = {}
			mainSize.width = hwg.win.gameField.offsetWidth;
			mainSize.height = hwg.win.gameField.offsetHeight;
			mainSize.kof = mainSize.height / mainSize.width;
			mainSize.min = parseInt(hwg.bubblesSize.min * mainSize.kof);
			mainSize.max = parseInt(hwg.bubblesSize.max * mainSize.kof);
		var top = get_random_flo(mainSize.max,90);
		var left = get_random_flo(1,100-mainSize.max);
		var width = get_random_int(mainSize.min,mainSize.max);
		
		posX && (top = posX);
		posY && (left = posY);
		
		var bobble = document.createElement('div');
			bobble.classList.add('hw-one-bubble');
			bobble.style.top = top+'%';
			bobble.style.left = left+'%';
			bobble.style.width = width+'%';
			bobble.dataset.id = ide;
			
		var booble_wrap = document.createElement('div');
			booble_wrap.style.animation = 'create-anim 0.4s';
			booble_wrap.classList.add('hw-one-bubble_wrap');
			
		var booble_img = document.createElement('div');
			booble_img.classList.add('hw-one-bubble_img');
			booble_img.style.backgroundImage ='url("'+hwg.sprites.dir+hwg.sprites.arr[get_random_int(0,hwg.sprites.arr.length)]+'")';
			
		var booble_img_boom = document.createElement('div');
		
		var typeBoom = get_random_int(0,hwg.sprites.boom.length);
			
			booble_img_boom.dataset.anim = hwg.sprites.boom[typeBoom].type;
			booble_img_boom.classList.add('hw-one-bubble_img_boom');
			booble_img_boom.style.backgroundImage ='url("'+hwg.sprites.dir+hwg.sprites.boom[typeBoom].img+'")';
		
		if (hwg.sprites.boom[typeBoom].sound){//sound
			var booble_sound_boom = document.createElement('audio');
				booble_sound_boom.src = hwg.sprites.boom[typeBoom].sound;
			booble_wrap.appendChild(booble_sound_boom);
		}
		
		var score = parseInt((1*kofSpeed)+(hwg.bubblesSize.max/width));
		
		bobble.addEventListener('pointerdown',click_bubble);
		bobble.addEventListener('touchend',click_end_bubble);
		bobble.addEventListener('mousedown',click_end_bubble);
		booble_wrap.appendChild(booble_img);
		booble_wrap.appendChild(booble_img_boom);
		bobble.appendChild(booble_wrap);
		hwg.win.gameField.appendChild(bobble);
		
		hwg.bubbles[ide]={elem:bobble,top:top,left:left,kof:kofSpeed,id:ide,score:score};
	}
	
	// если мы нажали на пузырёе
	function click_bubble(){	
		if (hwg.stat != 'play'){star_game();}
		var elem = this;
		var control = elem.dataset.control;
		
		if (!control) {
			elem.dataset.control = 'active';
			elem.style.zIndex = '-1';
			var width = parseInt(elem.style.width);
			var key = elem.dataset.id
			var booble = elem.querySelector('.hw-one-bubble_img');
			var booble_boom = elem.querySelector('.hw-one-bubble_img_boom');
			var typeAnim = booble_boom.dataset.anim;
			
				booble.style.opacity = 0;
				booble.style.width = '120%';
				booble.style.height = '120%';

				booble_boom.style.animation = typeAnim+'-anim 0.4s';
			
			setTimeout(function(){
				delete hwg.bubbles[key];
				elem.style.display = 'none';
				elem.remove()
			},400)
			
			if (hwg.bubbles[key]){
				if (!hwg.score) {hwg.score = 0;}
				if(hwg.score === NaN){hwg.score = 0;}
				hwg.score += parseInt(hwg.bubbles[key].score) || 0;
				hwg.win.gameScore.innerHTML = hwg.score;
			}
		}
	}
	function click_end_bubble(){
		var elem = this;
		var sound = elem.querySelector('audio');
		if (hwg.sprites.soundControl == 'on'){sound.play();}
	}

	// очищает игровое поле
	function clear_pole(){
		hwg.win.gameField.innerHTML='';
		hwg.bubbles = {};
	}
	
	// Функция тика, онаже ФПС отрисовки кадров, с доп переменными для неё
	hwg.timer.bufferTime = 0; // буферное время между тиками фреймов
	hwg.timer.bufferTimePage = 0; // Время со старта страници
	hwg.timer.kofFps = 1000/hwg.fps;
	function tick(fc){
		if (hwg.stat == 'play'){
			hwg.timer.bufferTime = hwg.timer.time
			hwg.timer.bufferTimePage = performance.now()
			const startBuff = hwg.timer.time
			const dopBuff = hwg.timer.bufferTimePage

			function step(time) {
				hwg.idTick = requestAnimationFrame(step);
				if (hwg.stat == 'play'){
					hwg.timer.bufferTime = time - hwg.timer.bufferTimePage;

					if(hwg.timer.bufferTime > hwg.timer.kofFps){
						hwg.timer.bufferTimePage = performance.now()
						hwg.timer.time = parseInt(time - dopBuff + startBuff)
						fc(hwg.timer.time)					
					}
					
				} else {cancelAnimationFrame(hwg.idTick);}
			}
			hwg.idTick = requestAnimationFrame(step);
		}
	}

	// функция раскрытияна на весь экран
	function fullScreen(type) {
		var element = document.documentElement;

		if (type == 'open'){
			if(element.requestFullscreen) element.requestFullscreen();
			else if(element.mozRequestFullScreen) element.mozRequestFullScreen();
			else if(element.webkitRequestFullscreen) element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
			else if(element.msRequestFullscreen) element.msRequestFullscreen();
		}
		
		if (type == 'clouse'){
			if (document.cancelFullScreen) document.cancelFullScreen();
			else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
			else if (document.webkitCancelFullScreen)document.webkitCancelFullScreen();
			else if (document.msCancelFullScreen)document.msCancelFullScreen();
		}		
	}
	//нажатие на кнопку меню "назад"
	function click_but_menuBack(){
		hwg.win.menuWin.classList.remove('active');
		hwg.win.menu.forEach(function(elem){
			elem.classList.remove('active');
		});
	}
	// нажатие на кнопку меню "Рейтинг"
	function click_but_menuRating(){
		var elem = hwg.win.rating;
		var elemclouse = hwg.win.ratingClouse;
		var active = elem.classList.contains('active')
		elem.innerHTML = "";
		if (active){ // если класс есть
			elem.classList.remove('active');
			elemclouse.classList.remove('active');
		}else {
			create_rating();
			elem.classList.add('active');
			elemclouse.classList.add('active');
		}
		
		function create_rating(){
			var ul = document.createElement('ul');
			var fulUrl = hwg.ajaxUrl.server+hwg.ajaxUrl.ratingList;
			let wievBloclInfo = '<div class="rating-dop-info">TOP : 50</div>'
			
			elem.innerHTML = wievBloclInfo;
			
			fetch(fulUrl).then(res => res.json()).then(result => {
				if(result.res && result.info.length > 0) {
					result.info.forEach(elem=>{
						let li = document.createElement('li');
						li.classList.add('t-center')
						li.innerHTML = `<span class="name">${elem.name}</span> : <span class="score">${elem.value}</span>`;
						ul.appendChild(li);
					})
					elem.appendChild(ul)
				}
			}).catch(err=>{
				console.log('ошибка сервер недоступен')
				let li = document.createElement('li');
				li.classList.add('t-red')
				li.classList.add('t-center')
				li.innerHTML = LANG.server_no_connect[hwg.lang];
				ul.appendChild(li);
				elem.appendChild(ul)
			});
		}
	}
	
	// Закрывает рейтинг
	function click_but_clouseRating(){
		var elem = hwg.win.rating;
		var elemclouse = hwg.win.ratingClouse;
		var active = elem.classList.contains('active')
		elem.innerHTML = "";
		if (active){ // если класс есть
			elem.classList.remove('active');
			elemclouse.classList.remove('active');
		}else {
			create_rating();
			elem.classList.add('active');
			elemclouse.classList.add('active');
		}
	}
	// нажатие на кнопку меню "Звук"
	function click_but_menuSound(){
		var active = this.classList.contains('active')
		if (active){ // если класс есть
			hwg.sprites.soundControl = 'on';
			this.querySelector('span').innerHTML = "ON";
			this.classList.remove('active');
		}else {// если класса нету
			hwg.sprites.soundControl = 'off';
			this.querySelector('span').innerHTML = "OFF";
			this.classList.add('active');
		}
	}	

	// нажатие на кнопку меню "Музыка"
	function click_but_menuMusick(){
		var active = this.classList.contains('active')
		if (active){ // если класс есть
			hwg.musickControl = 'on';
			this.querySelector('span').innerHTML = "ON";
			this.classList.remove('active');
			hwg.audio.fone.play()
		}else {// если класса нету
			hwg.musickControl = 'off';
			this.querySelector('span').innerHTML = "OFF";
			this.classList.add('active');
			hwg.audio.fone.pause()
		}
		localStorage.setItem('musick_сontrol',hwg.musickControl);
	}	
	
	// нажатие на кнопку меню "Язык"
	function click_but_lang(){
		var lang = this.dataset.lang
		
		switch (lang) {
			case 'ru': hwg.lang = 'en'; break;
			case 'en': hwg.lang = 'kz'; break;
			case 'kz': hwg.lang = 'ru'; break;
		}
				
		localStorage.setItem('lang',hwg.lang);
		correct_lang();
	}
	//Нажатие на кнопку меню "Выход"
	// function click_but_menuExit(){location.href = '/';}

	//кнопки финишного меню 
	function click_but_finisScore (){}
	function click_but_finisName (){}
	function click_but_finisRating (){
		let elem = this;
		var name = hwg.namePlayer || hwg.win.finisName.querySelector('input').value;
		var pattern = /^[A-Za-zА-ЯЁа-яё0-9_-]+$/;
		
		if(pattern.test(name)){
			let firstReg = ''
			if(!hwg.namePlayer){firstReg = '&f=1'}
			
			hwg.win.loaderDisplay.classList.add('active')
			
			let fulUrl = hwg.ajaxUrl.server+hwg.ajaxUrl.ratingAdd+'?name='+name+'&val='+hwg.scoreAjax+firstReg;
			setTimeout(()=>{ 
			fetch(fulUrl).then(res => res.json()).then(result => {
				if(result.res) {
					hwg.win.finisError.innerHTML = LANG.you_rating[hwg.lang];
					hwg.win.finisError.classList.remove('error');
					hwg.win.finisError.classList.add('yes');
					hwg.win.finisError.style.opacity = 1;
					hwg.win.finisName.style.display = 'none';
					hwg.win.finisRating.style.display = 'none';
					localStorage.setItem('name_player',name);
					hwg.namePlayer = name;
				}
				else {
					hwg.win.finisError.innerHTML = LANG.rating_server_name_error[hwg.lang];
					hwg.win.finisError.classList.remove('yes');
					hwg.win.finisError.classList.add('error');
					hwg.win.finisError.style.opacity = 1;
					clearTimeout(hwg.win.finisError.timerC);
					hwg.win.finisError.timerC = setTimeout(function(){hwg.win.finisError.style.opacity = 0;},4000);
				}

				hwg.win.loaderDisplay.classList.remove('active')
			});},1000)
		} else {
			hwg.win.finisError.innerHTML = LANG.rating_name_error[hwg.lang];
			hwg.win.finisError.classList.remove('yes');
			hwg.win.finisError.classList.add('error');
			hwg.win.finisError.style.opacity = 1;
			clearTimeout(hwg.win.finisError.timerC);
			hwg.win.finisError.timerC = setTimeout(function(){hwg.win.finisError.style.opacity = 0;},4000);
		}
	}
	function click_but_finisBack (){hwg.win.finishWin.classList.remove('active');}

	// Кнопка нажатия на молнию
	function click_but_lightning() {
		let elem = this;
		if(!elem.classList.contains('disable') && hwg.scils.lightning.count > 0){
			let sizeWindow = hwg.win.mainG.getBoundingClientRect() 
			let block = hwg.win.animlightningBlock;
				block.classList.add('active');
			let score = 0;
			let sizeblock = block.getBoundingClientRect()
			
			
			for (let key in hwg.bubbles) {
				let elem = hwg.bubbles[key].elem;
				let left = elem.offsetLeft
				let width = elem.offsetWidth
				if(left+width > sizeblock.left && left < sizeblock.left+sizeblock.width){
					score+= parseInt(hwg.bubbles[key].score);
					delete hwg.bubbles[key];
					elem.remove();
				}
			}
			
			if (hwg.sprites.soundControl == 'on'){
				hwg.scils.lightning.audio.pause();
				hwg.scils.lightning.audio.currentTime = 0;
				hwg.scils.lightning.audio.play();
			}
			
			hwg.score += parseInt(score); 
			hwg.win.gameScore.innerHTML = hwg.score;
			pause_game(false)
			
			clearTimeout(hwg.scils.lightning.tieid);
			hwg.scils.lightning.tieid = setTimeout(()=>{
				 star_game()
				 block.classList.remove('active');
			},hwg.scils.timerStop);

			hwg.scils.lightning.count--;
			localStorage.setItem('lightning_count', hwg.scils.lightning.count);
			elem.querySelector('.count').innerHTML = hwg.scils.lightning.count;
			elem.classList.add('disable')
		}
	}
	
	// Кнопка нажатия на линию
	function click_but_incision () {
		let elem = this;
		if(!elem.classList.contains('disable') && hwg.scils.incision.count > 0){
			let sizeWindow = hwg.win.mainG.getBoundingClientRect() 
			let score = 0;
			let block = hwg.win.animIncisionBlock;
				block.classList.add('active');
			let sizeblock = block.getBoundingClientRect()
			
			for (let key in hwg.bubbles) {
				let elem = hwg.bubbles[key].elem;
				let topE = elem.offsetTop 
				let height = elem.offsetHeight
				if(topE+height > sizeblock.top  && topE < sizeblock.top+sizeblock.height){
					score+= parseInt(hwg.bubbles[key].score);
					delete hwg.bubbles[key];
					elem.remove()
				}
			}
			
			if (hwg.sprites.soundControl == 'on'){
				hwg.scils.incision.audio.pause();
				hwg.scils.incision.audio.currentTime = 0;
				hwg.scils.incision.audio.play();
			}
			
			hwg.score += parseInt(score); 
			hwg.win.gameScore.innerHTML = hwg.score;
			pause_game(false)
			
			let controlTimer = 0;
			clearTimeout(controlTimer);
			controlTimer = setTimeout(()=>{
				 star_game()
				 block.classList.remove('active');
			},hwg.scils.timerStop);

			hwg.scils.incision.count--;
			localStorage.setItem('incision_count', hwg.scils.incision.count); 
			elem.querySelector('.count').innerHTML = hwg.scils.incision.count;
			elem.classList.add('disable')
		}
	}
	
	// Кнопка нажатия на взрыв
	function click_but_explosion() {
		let elem = this;
		if(!elem.classList.contains('disable') && hwg.scils.explosion.count > 0){
			let score = 0;
			let sizeWindow = hwg.win.mainG.getBoundingClientRect() 
			let block = hwg.win.animExplosionBlock;
				block.classList.add('active');
			let blockH = block.offsetHeight
				block.style.width = blockH+'px';
				block.style.left = 'calc(50% - '+(blockH/2)+'px)';
			let sizeblock = block.getBoundingClientRect()

			for (let key in hwg.bubbles) {
				let elem = hwg.bubbles[key].elem;
				let left = elem.offsetLeft
				let topE = elem.offsetTop 
				let width = elem.offsetWidth
				let height = elem.offsetHeight
				if(
					topE+height > sizeblock.top 
					&& topE < sizeblock.top+sizeblock.height
					&& left+width > sizeblock.left
					&& left < sizeblock.left+sizeblock.width
				) {
					score+= parseInt(hwg.bubbles[key].score);
					delete hwg.bubbles[key];
					elem.remove()
				}
			}
			
			if (hwg.sprites.soundControl == 'on'){
				hwg.scils.explosion.audio.pause();
				hwg.scils.explosion.audio.currentTime = 0;
				hwg.scils.explosion.audio.play();
			}
			
			hwg.score += parseInt(score); 
			hwg.win.gameScore.innerHTML = hwg.score;
			pause_game(false)
			
			let controlTimer = 0;
			clearTimeout(controlTimer);
			controlTimer = setTimeout(()=>{
				star_game()
				block.classList.remove('active');
			},hwg.scils.timerStop);

			hwg.scils.explosion.count--;
			localStorage.setItem('explosion_count', hwg.scils.explosion.count);
			elem.querySelector('.count').innerHTML = hwg.scils.explosion.count;
			elem.classList.add('disable')
		}
	}
	// кнопка закрытия модалного окна 
	function click_but_clouse_modal(){
		let elem = this;
		let modalBlock = elem.closest('.hw-modal'); 
			modalBlock.classList.remove('active')
	}
	
	// Нажатие на кнопку информации об игре
	function click_btn_info(){
		hwg.win.modalInfo.classList.add('active')
	}
	
	// Нажатие на кнопку смотреть рекламу
	function click_but_commercial(){
		let counterB = hwg.win.commercialBlock.querySelector('.modal-counter')
		let contentB = hwg.win.commercialBlock.querySelector('.modal-content')
		let timerInt = 10
		let timerId = 0
		
		pause_game()
		hwg.win.commercialBlock.classList.add('active')
		contentB.innerHTML = LANG.commercial_title[hwg.lang];
		counterB.innerHTML = timerInt
		
		timerId = setInterval(()=>{
			if (timerInt <= 0){
				counterB.innerHTML = '✕'
				counterB.classList.add('active')
				clearInterval(timerId)
			}
			else {
				timerInt--
				counterB.innerHTML = timerInt
			}
		},1000);
	}
	
	// закрывает рекламу 
	function click_btn_clouse_commercial(){
		let elem = this;
		if(elem.classList.contains('active')){
			hwg.win.commercialBlock.classList.remove('active')
			hwg.win.commercialBlock.querySelector('.modal-content').innerHTML = '';
			add_random_skills()
			elem.classList.remove('active')
		}
	}
	
	// добавит 1 случайный скил 
	function add_random_skills(){
		let skill = get_random_int(1,100);
		let contentB = hwg.win.addSkillBlock.querySelector('.modal-content')
		let newSkillB = hwg.win.addSkillBlock.querySelector('.new-skill')
		let newSkill = '';
		
		switch (true){
			case (skill > 70): 
				newSkill = '<span class="btn explosion">✹</span>' ;
				localStorage.setItem('explosion_count', parseInt(hwg.scils.explosion.count)+1);
			break;
			case (skill > 35): 
				newSkill = '<span class="btn incision">↭</span>' ; 
				localStorage.setItem('incision_count', parseInt(hwg.scils.incision.count)+1);
			break;
			case (skill > 0): 
				newSkill = '<span class="btn no_p lightning">↯</span>' ;
				localStorage.setItem('lightning_count', parseInt(hwg.scils.lightning.count)+1);
			break;
		}
		
		set_count_item_skills();
		hwg.win.addSkillBlock.classList.add('active')
		newSkillB.innerHTML = newSkill+'<span class="dop-text"> + 1 !!!!! </span>';
	}

	// Helper FCN
	
	// вернет случайное число от МАКСЕМУМА до МИНЕМУМА формата INT 10
	function get_random_int(max,min){return Math.floor(Math.random() * (max - min+1)) + min;}	
	// вернет случайное число от МАКСЕМУМА до МИНЕМУМА формата FLOOT 10.241
	function get_random_flo(max,min){return Math.random() * (max - min) + min;}
}