"use strict";
var LANG = {};

LANG.btn_back = {
	'ru':'Назад',
	'en':'Back',
	'kz':'Артқа'
}
LANG.btn_rating = {
	'ru':'Рейтинг',
	'en':'Rating',
	'kz':'Рейтинг'
}
LANG.btn_sound = {
	'ru':'Звук',
	'en':'Sound',
	'kz':'Дыбыс'
}
LANG.btn_musick = {
	'ru':'Музыка',
	'en':'Music',
	'kz':'Музыка',
}
LANG.server_no_connect = {
	'ru':'Сервер недоступен',
	'en':'Server is not available',
	'kz':'Сервер қолжетімді емес'
}
LANG.btn_go_rating = {
	'ru':'В Рейтинг',
	'en':'Go Rating',
	'kz':'Рейтингке қосу'
}
LANG.btn_lang = {
	'ru':'Язык',
	'en':'Language',
	'kz':'Тіл',
}
LANG.btn_get_skill = {
	'ru':'Получить',
	'en':'Get',
	'kz':'Алу'
}
LANG.modal_info = {
	'ru':`<h2>Цель :</h2>
		<p>За отведённое время лопнуть как можно больше пузырей, и занять свой топ в рейтинге !!</p>
		<br>
		<h2>Скилы :</h2>
		<p>В каждом раунде скилл можно использовать лишь 1 раз. Делайте это обдуманно ;)</p>`,
	'en':`<h2>Your aim :</h2>
		<p>Burst as many bubbles as possible in the allotted time and take your top in the ranking !!</p>
		<br>
		<h2>Skills :</h2>
		<p>The skill can only be used once per round. Do it mindfully ;)</p>`,	
	'kz':`<h2>Мақсат :</h2>
		<p>Бөлінген уақытта мүмкіндігінше көп көпіршіктерді жарып, рейтингте өз шыңыңызды алыңыз !!</p>
		<br>
		<h2>Дағдылар :</h2>
		<p>Әр раундта шеберлікті тек бір рет қолдануға болады. Ақылмен жасаңыз;)</p>`,
}
LANG.you_rating = {
	'ru':'Вы в Рейтинге !',
	'en':'You are in the Ranking!',
	'kz':'Сіз рейтингтесіз!',
}
LANG.rating_name_error = {
	'ru':'Неверное имя!!!',
	'en':'Wrong name!!!',
	'kz':'Аты қате!!!',
}
LANG.rating_server_name_error = {
	'ru':'Это имя занято',
	'en':'This name is taken',
	'kz':'Бұл атау алынған',
}
LANG.commercial_title = {
	'ru':'ТУТ БУДЕТ РЕКЛАМА',
	'en':'HERE WILL BE ADVERTISING',
	'kz':'ОСЫНДА ЖАРНАМА БОЛАДЫ',
}

